# CloudFormation

This is a resource management service that is used to define the current state of the application infrastructure.

It allows the developers to define the resources, their type, count and the connections between them via JSON / YAML templates. These templates can be updated whenever, the state of the resources needs to be altered.

Moreover, we can have different templates per region.

Resources available on CloudFormation

## Stacks and Stacksets