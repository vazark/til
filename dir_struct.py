import glob
import os

root_dir = './src'
proj_name = 'TIL'

# Program version.
__VERSION__ = '1.0.0'
"""
For version 2 -> update inplace instead of regeneration

Update logic:

- Parse the MD file and hold in memory the order & hierarchy of items
- Glob all files in src/ and compare against in-memory structure
- update the in-mem structure only for any changes in file/folder names without altering hierarchy
- Store as MD
- User has to manually update the summary file to ensure that new folders and files are in the desired order
"""

def tree(path, depth=0):
    files = []
    dirs = []
    for entry in os.listdir(path):
        new_path = os.path.join(path, entry)
        if not entry.startswith('.'):
            if os.path.isdir(new_path):
                inner_folders = glob.glob(r"{}/**/*.md".format(new_path), recursive=True)
                if len(inner_folders) > 1:
                    dirs.append((entry, new_path))
            else:
                files.append((entry, new_path))

    for (entry, new_path) in files:
        filename, ext = os.path.splitext(entry)
        if ext != '.md' or filename == 'SUMMARY' or filename == 'index':
            continue
        path_to_save = os.path.relpath(new_path, root_dir)
        print('  ' * depth, end='')
        print('- [{}](<./{}>)'.format(filename, path_to_save))

    for (entry, new_path) in dirs:
        # Check if index exists,
        readme_path = os.path.join(new_path,'index.md')
        print('  ' * depth, end='')
        if os.path.exists(readme_path):
            index_path = os.path.relpath(readme_path, root_dir)
            print('- [{}](<./{}>)'.format(entry, index_path))
        else:
            print('- [{}]()'.format(entry))
        tree(new_path, depth + 1)

def main():
    print('# Summary\n')

    index_readme = os.path.join(root_dir, 'index.md')
    if index_readme:
        print('[{}](<./{}>)'.format(proj_name, 'index.md'))

    tree(os.path.expanduser(root_dir))


# Run this when invoked directly
if __name__ == '__main__':
    main()